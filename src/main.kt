fun main(args: Array<String>) {

    /*
    * 1- Crear una clase Persona
    *   1.1- Añadir atributos
    *   1.2- Añadir métodos
    * 2- Crear una instacia de la clase
    * 3- Llamar métodos desde la(s) instancia(s) creada(s)
    * 4- Demostración de getters y setters
    * 4- Crear una clase que herede de Persona
    *   4.1- Crear clase Alumno
    *   4.2- Añadir constructores
    *   4.3- Hacer una modificación a método padre
    */

    //personas()
    alumnos()

}


fun personas() {

    //Creamos instancias de la clase Persona
    val ernesto: Persona = Persona("Ernesto", "Gálvez", 23, 'M')
    val alan: Persona = Persona("Alan", "Gálvez")
    val alguien: Persona = Persona()



    //Añadimos o modificamos los atributos con los getters y setters
    alguien.nombre      = "Fulana"          //Agrega a alguien el nombre
    alguien.apellido    = "Torres"          //Agrega a alguien el apellido
    alguien.edad        = 16                //Agrega a alguien su edad
    alguien.genero      = 'F'               //Agrega a alguien su género

    alan.edad           = 25                //Agrega a alan su edad
    alan.genero         = 'M'               //Agrega a alan su género

    println("La edad de Ernesto es: ${ernesto.edad}")


    //Llamando a los métodos de personas
    ernesto.imprimir()                      //Muestra las Personas creadas
    alan.imprimir()                         //Muestra las Personas creadas
    alguien.imprimir()                      //Muestra las Personas creadas
    ernesto.esMayorDeEdad()                 //Verifica si ernesto es mayor de edad
    ernesto.esMayorQue(alguien)             //Verifica si ernesto es mayor que alguien
    alguien.esMayorQue(ernesto)             //Verifica si alguien es mayor que ernesto

    //Cambiando los atributos desde el teclado
    ernesto.setNombreKB()
    ernesto.setApellidoKB()
    ernesto.imprimir()
    alguien.setEdadKB()
    alguien.setGeneroKB()
    alguien.imprimir()
}


fun alumnos() {

    val alumno1 = Alumno("Ernesto", "Gálvez", 23, 'M', 7.89)
    val alumno2 = Alumno()

    alumno1.imprimir()
    alumno1.setCalificacionKB()
    alumno1.imprimir()

    alumno2.imprimir()
    alumno2.calificacion = 10.0
    alumno2.imprimir()

}